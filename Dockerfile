#set base image
FROM python:3.4
#set working dir
WORKDIR /code
#copy depedencies
COPY requirements.txt .
#install depedencies
RUN pip install -r requirements.txt
COPY src/ .
CMD [ "python3", "./palindrome.py" ]
